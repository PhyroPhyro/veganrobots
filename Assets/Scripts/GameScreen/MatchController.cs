﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.EventSystems;

public class MatchController : Singleton<MatchController>
{
    [HideInInspector] public bool RoundIsOn;
    [HideInInspector] public bool EndGame;
    [HideInInspector] private int RoundNumber;
    [SerializeField] private PlayerScore LastRoundWinner;

    private Vector2 initPos;
    public Transform SpawnController;
    public GameObject buttonEndGame;
    public List<GameObject> spawnPoints = new List<GameObject>();
    public List<GameObject> itemPool = new List<GameObject>();
    public List<PlayerScore> Scores =  new List<PlayerScore>();

    public GameObject ScoreCanvas;
    public GameObject TutorialCanvas;

    void Start()
    {
        PlayerCanControl(false); 

        foreach(var _player in GameController.Instance.playerList)
            Scores.Add(new PlayerScore() { playerName = _player.gameObject, score = 0 });

        foreach(Transform _points in SpawnController)
            spawnPoints.Add(_points.gameObject);

        initPos = GameController.Instance.playerList[0].transform.position;

        SoundManager.Instance.PlayBGM("LevelMusic");

        TutorialCanvas.SetActive(true);

        //Invoke("PopulateArena", 2);// Game Start Delay
    }

    public void PopulateArena()
    {
        SoundManager.Instance.PlayBGM("LevelMusic");
        
        StartCoroutine(SetSpawnLocations());
    }

    IEnumerator SetSpawnLocations()
    {
        spawnPoints = spawnPoints.OrderBy( x => Random.value ).ToList( );

        ScoreCanvas.gameObject.SetActive(false);

        yield return new WaitForSeconds(1);

        for(var i = 0; i < GameController.Instance.playerList.Count; i++)
        {
            GameController.Instance.playerList[i].transform.position = spawnPoints[i].transform.position;
            //play summon field/energy animation

            GameController.Instance.playerList[i].GetComponent<PlayerController>().RoundStart(); // RESET ROBOT ENERGY

            yield return new WaitForSeconds(0.5f);
        }

        SpawnItem(1);

        yield return new WaitForSeconds(2);

        // Call GO warning        

        yield return new WaitForSeconds(1);

        StartGame();
        StartCoroutine(ItemTimer());
    }

    void StartGame()
    {
        RoundIsOn = true;

        // ENABLE PLAYER CONTROL
        PlayerCanControl(true);   
    }

    IEnumerator ItemTimer()
    {
        yield return new WaitForSeconds(8);
        SpawnItem(1);

        StartCoroutine(ItemTimer());
    }

    public void RoundEnd(GameObject _player)
    {
        RoundIsOn = false;
        RoundNumber++;

        for(var i = 0; i < GameController.Instance.playerList.Count; i++)
        {
            if(Scores[i].playerName == _player.gameObject)
                LastRoundWinner = Scores[i];
        }

        var scorer = LastRoundWinner.score++;
        
        if(scorer >= 2)
        {
            if(EndGame) return;
            EndMatch();
            buttonEndGame.SetActive(true);
            FindObjectOfType<EventSystem>().SetSelectedGameObject(buttonEndGame);
            EndGame = true;
        }

        StopCoroutine(ItemTimer());

        ScoreCanvas.gameObject.SetActive(true);

        if(EndGame) return;
        
        Debug.Log("PLAYER " + LastRoundWinner.playerName + ", SCORE: " + LastRoundWinner.score);

        // Remove players from screen
        for(var i = 0; i < GameController.Instance.playerList.Count; i++)
            GameController.Instance.playerList[i].transform.position = initPos;
            
        PlayerCanControl(false);
        
        Invoke("PopulateArena", 2);
    }

    public void EndMatch()
    {
        Debug.Log("PLAYER " + LastRoundWinner.playerName + " VENCEU !!!");
    }

    void PlayerCanControl(bool controlState){
        // DISABLE PLAYER CONTROL
        for(var i = 0; i < GameController.Instance.playerList.Count; i++)
        {
            //GameController.Instance.playerList[i].GetComponent<PlayerPlatformerController>().enabled = controlState;
            GameController.Instance.playerList[i].GetComponent<PlayerController>().enabled = controlState;
        } 
    }

    public void SpawnItem(int quantity)
    {
        for(var it = 0; it < quantity; it++)
        {        
            var itemPicked = ItemController.Instance.PickupItemList[Random.Range(0, ItemController.Instance.PickupItemList.Count)];
            var itemSpawned = Instantiate(itemPicked, spawnPoints[GameController.Instance.playerList.Count + it].transform.position, Quaternion.identity);
        }
        spawnPoints = spawnPoints.OrderBy( x => Random.value ).ToList( );
    }
}

public class PlayerScore
{
    public GameObject playerName;
    public int score;
}