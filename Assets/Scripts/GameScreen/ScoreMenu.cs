﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreMenu : MonoBehaviour
{
    public List<GameObject> portraits = new List<GameObject>();
    public TextMeshProUGUI _text;

    void Awake()
    {
        foreach(Transform child in transform)
        {
            portraits.Add(child.gameObject);
        }
    }

    void OnEnable()
    {
        for(var p = 0; p < portraits.Count; p++)
        {
            for(var i = 0; i < MatchController.Instance.Scores[p].score; i++)
            {
                portraits[p].transform.GetChild(i).gameObject.SetActive(true);
            }
        }

        if(MatchController.Instance.EndGame)
            _text.text = "Fim de Jogo";
    }
}