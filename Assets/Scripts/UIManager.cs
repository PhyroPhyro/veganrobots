﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIManager : MonoBehaviour
{

    public GameObject OptionCanvas;
    public Button SelectMenuButton, SelectCreditsButton;

    private void Update()
    {
        if (Input.GetButtonDown("Horizontal"))
            SoundManager.Instance.PlaySFX("Click");
    }

    private void Start()
    {
        SoundManager.Instance.PlayBGM("MainMusic");
    }

    public void OpenOptions()
    {
        OptionCanvas.SetActive(true);
        FindObjectOfType<EventSystem>().SetSelectedGameObject(SelectCreditsButton.gameObject);
    }

    public void CloseOptions()
    {
        OptionCanvas.SetActive(false);
        FindObjectOfType<EventSystem>().SetSelectedGameObject(SelectMenuButton.gameObject);
    }

    public void ExitGame()
    {
        Debug.Log("Exit Game");
    }

}