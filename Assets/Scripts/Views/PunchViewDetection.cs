﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunchViewDetection : MonoBehaviour
{
    public PlayerView playerView;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "player")
        {
            collision.GetComponent<PlayerController>().GetHit(playerView.transform.position, GameController.Instance.PunchForce);
        }
    }

    public IEnumerator PunchCoroutine()
    {
        gameObject.SetActive(true);
        yield return new WaitForSeconds(GameController.Instance.PunchTime);
        gameObject.SetActive(false);
    }
}
