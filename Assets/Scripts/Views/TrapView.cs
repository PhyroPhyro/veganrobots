﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapView : Item
{
    public float TimeToFade;
    public float TimeTraped;
    public float EnergyDrained;
    public RemoteTrigger triggerObject;
    private bool canTrap;

    IEnumerator Start()
    {
        triggerObject.OnTriggerEnter += Execute;
        yield return new WaitForSeconds(TimeToFade);
        triggerObject.gameObject.SetActive(true);
        GetComponent<SpriteRenderer>().enabled = false;
        canTrap = true;
    }

    private void Execute(Collider2D collision)
    {
        if (collision.tag == "player" && canTrap)
        {
            collision.GetComponent<PlayerController>().GetHit(transform.position, 0, TimeTraped);
            collision.GetComponent<PlayerController>().EnergyAmount -= EnergyDrained;

            SoundManager.Instance.PlaySFX("Trap");
            GetComponent<Animator>().SetTrigger("explode");

            StartCoroutine(DelayDestroy());
        }
    }

    IEnumerator DelayDestroy()
    {
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }
}
