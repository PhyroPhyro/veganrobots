﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryView : Item
{
    void Start()
    {
        PlayerController playerController = GameController.Instance.GetPlayerControllerById(PlayerId);
        if(playerController.LastCheckpoint >= GameController.Instance.RechargeCheckpoint.Count - 1)
        {
            playerController.EnergyAmount = GameController.Instance.MaxRechargeValue;
        }
        else
        {
            playerController.LastCheckpoint++;
            playerController.EnergyAmount = GameController.Instance.RechargeCheckpoint[playerController.LastCheckpoint];
        }

        SoundManager.Instance.PlaySFX("Battery");
        Destroy(gameObject);
    }
}
