﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GranadeView : Item
{
    public float TimeToExplode;
    public float ExplosionForce;
    public RemoteTrigger triggerObject; 
    
    IEnumerator Start()
    {
        SoundManager.Instance.PlaySFX("GranadeThrow");

        triggerObject.OnTriggerEnter += Explode;
        yield return new WaitForSeconds(TimeToExplode);

        triggerObject.gameObject.SetActive(true);
    }

    private void Explode(Collider2D collider)
    {
        if(collider.tag == "player" && collider.GetComponent<PlayerController>().PlayerId != PlayerId)
        {
            collider.GetComponent<PlayerController>().GetHit(transform.position, ExplosionForce);
        }

        SoundManager.Instance.PlaySFX("GranadeExplode");

        GetComponent<Animator>().SetTrigger("explode");

        StartCoroutine(DelayDestroy());
    }

    IEnumerator DelayDestroy()
    {
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }
}
