﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPlatformerController : PhysicsObject
{
    public float maxSpeed = 7;
    public float jumpTakeOffSpeed = 7;

    private SpriteRenderer spriteRenderer;
    private Animator animator;

    private PlayerController playerController;

    // Use this for initialization
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();

        playerController = GetComponent<PlayerController>();
    }

    protected override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;

        if(playerController.isHit || !MatchController.Instance.RoundIsOn)
            move.x = 0;
        else
            move.x = Input.GetAxis(InputController.GetButtonByPlayerId(playerController.PlayerId, "Horizontal"));

        if (Input.GetButtonDown(InputController.GetButtonByPlayerId(playerController.PlayerId, "Fire1")) && grounded &&MatchController.Instance.RoundIsOn)
        {
            velocity.y = jumpTakeOffSpeed;
            SoundManager.Instance.PlaySFX("Jump");
        }
        else if (Input.GetButtonUp(InputController.GetButtonByPlayerId(playerController.PlayerId, "Fire1")) && MatchController.Instance.RoundIsOn)
        {
            if (velocity.y > 0)
            {
                velocity.y = velocity.y * 0.5f;
            }
        }

        bool flipSprite = spriteRenderer.flipX ? move.x < -0.01f : move.x > 0.01f;
        if (flipSprite)
        {
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }

        animator.SetBool("grounded", grounded);
        animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);

        targetVelocity = move * maxSpeed;
    }
}