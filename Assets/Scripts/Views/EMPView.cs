﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EMPView : Item
{
    public RemoteTrigger triggerObject;
    public float TimeToExplode;
    
    IEnumerator Start()
    {
        triggerObject.OnTriggerEnter += Explode;

        yield return new WaitForSeconds(TimeToExplode);

        triggerObject.gameObject.SetActive(true);
    }

    private void Explode(Collider2D collider)
    {
        if(collider.tag == "player" && collider.GetComponent<PlayerController>().PlayerId != PlayerId)
        {
            collider.GetComponent<PlayerController>().DisarmPlayer();
        }

        SoundManager.Instance.PlaySFX("EMP");

        Destroy(gameObject);
    }
}
