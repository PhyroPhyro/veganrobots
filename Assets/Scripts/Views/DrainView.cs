﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrainView : Item
{
    public float EnergyToDrain;
    public RemoteTrigger triggerObject;
    private Coroutine destroyRoutine;

    void Start()
    {
        triggerObject.OnTriggerEnter += Explode;
        triggerObject.gameObject.SetActive(true);
        SoundManager.Instance.PlaySFX("Drain");
    }

    private void Explode(Collider2D collider)
    {
        if (collider.tag == "player" && collider.GetComponent<PlayerController>().PlayerId != PlayerId)
        {
            collider.GetComponent<PlayerController>().EnergyAmount -= EnergyToDrain;
            GameController.Instance.GetPlayerControllerById(PlayerId).EnergyAmount += EnergyToDrain;
        }

        if(destroyRoutine == null)
        {
            GetComponent<Animator>().SetTrigger("explode");

            destroyRoutine = StartCoroutine(DelayDestroy());
        }       
    }

    IEnumerator DelayDestroy()
    {
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }
}
