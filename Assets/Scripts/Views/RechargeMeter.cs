﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RechargeMeter : MonoBehaviour
{
    public Slider rechargeSlider;

    void Start()
    {
        rechargeSlider.maxValue = GameController.Instance.MaxRechargeValue;
        rechargeSlider.minValue = 0;
        rechargeSlider.value = 0;

        GetComponent<PlayerController>().OnEnergyAmountChanged += SetSliderValue;
    }

    public void SetSliderValue(float value)
    {
        rechargeSlider.value = value;
    }
}
