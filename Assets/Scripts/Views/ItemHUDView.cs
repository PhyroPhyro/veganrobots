﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHUDView : MonoBehaviour
{
    public int PlayerId;
    private GameObject SpawnedItem;

    public void InstantiateItem(ItemTypes itemType)
    {
        if (SpawnedItem != null)
            DestroyItem();

        SpawnedItem = Instantiate(ItemController.Instance.PickupItemList.Find(i => i.itemType == itemType)).gameObject;
        SpawnedItem.transform.SetParent(transform);
        SpawnedItem.transform.localPosition = Vector3.zero;
        SpawnedItem.transform.localScale = Vector3.one *1.5f;
        SpawnedItem.GetComponent<Collider2D>().enabled = false;
        SpawnedItem.GetComponent<PickupItemView>().PlayerId = PlayerId;
    }

    public void DestroyItem()
    {
        Destroy(SpawnedItem);
    }
}
