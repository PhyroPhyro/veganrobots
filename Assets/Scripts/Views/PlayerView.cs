﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerView : MonoBehaviour {
    public delegate void OnRecharge();
    public event OnRecharge OnStartRecharge, OnStopRecharge;

    public Animator animator;
    public bool isPunching;
    public bool isUsingItem;
    public bool isWalking;
    public bool isCharging;
    public bool isInChargeZone;
    public bool isHit;
    public bool CanPunch = true;
    public GameObject leftSpawnPoint, rightSpawnPoint;
    public RechargeAuraView rechargeAura;
    public PunchViewDetection leftPunchView, rightPunchView;

    private Coroutine rechargeRoutine;
    private PlayerController playerController;
    private PlayerController currentAuraPlayerController;

	// Update is called once per frame
	void Start () {
        animator = GetComponent<Animator>();
        playerController = GetComponent<PlayerController>();
    }

    private void FixedUpdate()
    {
        if (GetComponent<PlayerPlatformerController>().velocity.x != 0)
        {
            isWalking = true;
            StopRecharge();
        }

        if (Input.GetButtonDown(InputController.GetButtonByPlayerId(playerController.PlayerId, "Fire2")) && MatchController.Instance.RoundIsOn)
        {
            if(CanPunch)
            {
                CanPunch = false;
                isPunching = true;
                ActivatePunch();
                StopRecharge();
            }
        }

        if (Input.GetButton(InputController.GetButtonByPlayerId(playerController.PlayerId, "Fire3")) && MatchController.Instance.RoundIsOn)
        {
            isUsingItem = true;
            ItemController.Instance.ExecuteItem(playerController.PlayerId);
            StopRecharge();
        }

        if (GetComponent<PlayerPlatformerController>().velocity.x == 0)
        {
            isWalking = false;
        }

        if (Input.GetButtonUp(InputController.GetButtonByPlayerId(playerController.PlayerId, "Fire2")))
        {
            isPunching = false;
        }

        if (Input.GetButtonUp(InputController.GetButtonByPlayerId(playerController.PlayerId, "Fire3")))
        {
            isUsingItem = false;
        }

        //Jump
        if(Input.GetButtonDown(InputController.GetButtonByPlayerId(playerController.PlayerId, "Fire1")))
        {     
            animator.SetTrigger("jump");
        }
    }

    private void StartRecharge()
    {
        rechargeRoutine = StartCoroutine(RechargeDelay());
        //rechargeAura.StartAura();
        //SoundManager.Instance.PlaySFX("Charge", true);
    }

    IEnumerator RechargeDelay()
    {
        yield return new WaitForSeconds(0.5f);
        if (!isCharging)
        {
            isCharging = true;
            animator.SetTrigger("energy");
            if (OnStartRecharge != null)
                OnStartRecharge();
        }
    }

    private void StopRecharge()
    {
        isCharging = false;
        isInChargeZone = false;
        if (OnStopRecharge != null) 
            OnStopRecharge();
        if(rechargeRoutine != null)
            StopCoroutine(rechargeRoutine);

        SoundManager.Instance.SfxSource.Stop();

        rechargeAura.StopAura();
    }

    private void ActivatePunch()
    {
        StartCoroutine(DelayPunchEffect());
        animator.SetTrigger("punch");
        SoundManager.Instance.PlaySFX("Punch");
    }

    IEnumerator DelayPunchEffect()
    {
        yield return new WaitForSeconds(0.2f);
        if (GetComponent<SpriteRenderer>().flipX)
            StartCoroutine(rightPunchView.PunchCoroutine());
        else
            StartCoroutine(leftPunchView.PunchCoroutine());

        if(currentAuraPlayerController != null)
        {
            currentAuraPlayerController.GetHit(transform.position, GameController.Instance.PunchForce);
            currentAuraPlayerController = null;
        }
        yield return new WaitForSeconds(0.5f);

        CanPunch = true;
    }

    public void GetHit(Vector2 fromPosition, float hitForce)
    {
        if(!isHit)
        {
            StopRecharge();
            animator.SetTrigger("hit");
            animator.SetBool("hitting", true);
            isHit = true;

            SoundManager.Instance.PlaySFX("Hit", true);

            playerController.Decharge(true);

            if (fromPosition.x > transform.position.x)
                hitForce = -hitForce;

            GetComponent<Rigidbody2D>().AddForce(new Vector2(hitForce, 0));
        }
    }

    public void RecoverFromHit()
    {
        animator.SetBool("hitting", false);
        SoundManager.Instance.SfxSource.Stop();
        isHit = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "item")
        {
            ItemController.Instance.GiveItemToPlayerById(other.GetComponent<PickupItemView>().itemType, GetComponent<PlayerController>().PlayerId);
            if(other.GetComponent<PickupItemView>().PlayerId != 0)
            {
                ItemController.Instance.TakeItemFromPlayerById(other.GetComponent<PickupItemView>().PlayerId);
            }
            else
            {
                other.GetComponent<PickupItemView>().DestroyItem();
            }
            SoundManager.Instance.PlaySFX("Pickup");
        }

        if(other.tag == "aura")
        {
            currentAuraPlayerController = other.GetComponent<RechargeAuraView>().playerController;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "charge" && !isInChargeZone && !isCharging && !isPunching && !isUsingItem && !isWalking && GetComponent<PlayerPlatformerController>().grounded)
        {
            isInChargeZone = true;
            StartRecharge();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "charge" && isCharging)
        {
            StopRecharge();
        }

        if (collision.tag == "aura")
        {
            currentAuraPlayerController = null;
        }
    }
}
