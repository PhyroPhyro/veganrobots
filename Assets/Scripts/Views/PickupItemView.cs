﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupItemView : MonoBehaviour
{
    public int PlayerId = 0;
    public ItemTypes itemType;

    public void DestroyItem()
    {
        Destroy(gameObject);
    }
}
