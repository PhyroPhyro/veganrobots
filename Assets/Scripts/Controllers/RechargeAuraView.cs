﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RechargeAuraView : MonoBehaviour
{
    private Coroutine auraRoutine;
    public float AuraIncreasingRate;
    public float MaxAuraSize;
    public PlayerController playerController;

    public void StartAura()
    {
        gameObject.SetActive(true);
        auraRoutine = StartCoroutine(AuraRoutine());
    }

    public void StopAura()
    {
        transform.localScale = Vector3.one;
        if(auraRoutine != null)
            StopCoroutine(auraRoutine);
        gameObject.SetActive(false);
    }

    IEnumerator AuraRoutine()
    {
        while(transform.localScale.x < MaxAuraSize)
        {
            transform.localScale += Vector3.one * AuraIncreasingRate;
            yield return null;
        }
    }
}
