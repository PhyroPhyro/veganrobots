﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : Singleton<GameController> {

    public enum GameState
    {
        Preparing,
        Playing,
        Ending
    }

    public float PunchForce, PunchTime, HitForceDeceleration, TimeStunedFromHit;
    public float MaxRechargeValue,RechargeRate, DechargeRate;

    public List<float> RechargeCheckpoint;

    public GameObject TutorialCanvas;

    public List<PlayerController> playerList = new List<PlayerController>();

    void Start()
    {
        Physics2D.IgnoreCollision(playerList[0].GetComponent<Collider2D>(), playerList[1].GetComponent<Collider2D>());
    }

    public PlayerController GetPlayerControllerById(int playerId)
    {
        return playerList.Find(p => p.PlayerId == playerId);
    }
}
