﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : Singleton<InputController>
{
    public static string GetButtonByPlayerId(int playerId, string buttonKey)
    {
        switch (playerId)
        {
            case 1:
                if (buttonKey == "Horizontal")
                    return "Horizontal";
                if (buttonKey == "Fire1")
                    return "Fire1";
                if (buttonKey == "Fire2")
                    return "Fire2";
                if (buttonKey == "Fire3")
                    return "Fire3";
                break;
            case 2:
                if (buttonKey == "Horizontal")
                    return "Horizontal2";
                if (buttonKey == "Fire1")
                    return "Fire1P2";
                if (buttonKey == "Fire2")
                    return "Fire2P2";
                if (buttonKey == "Fire3")
                    return "Fire3P2";
                break;
            case 3:
                if (buttonKey == "Horizontal")
                    return "Horizontal3";
                if (buttonKey == "Fire1")
                    return "Fire1P3";
                if (buttonKey == "Fire2")
                    return "Fire2P3";
                if (buttonKey == "Fire3")
                    return "Fire3P3";
                break;
            case 4:
                if (buttonKey == "Horizontal")
                    return "Horizontal4";
                if (buttonKey == "Fire1")
                    return "Fire1P4";
                if (buttonKey == "Fire2")
                    return "Fire2P4";
                if (buttonKey == "Fire3")
                    return "Fire3P4";
                break;
        }

        return "Horizontal";
    }
}
