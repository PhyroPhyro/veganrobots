﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemTypes
{
    Granade,
    Shield,
    Trap,
    Drain,
    Battery,
    EMP
}

public class ItemController : Singleton<ItemController>
{
    public List<Item> itemList;
    public List<ItemHUDView> itemHUDList;
    public List<PickupItemView> PickupItemList;
    public Dictionary<int, ItemTypes> playerInventory = new Dictionary<int, ItemTypes>();

    public float ThrowableForce;

    public void GiveItemToPlayerById(ItemTypes itemType, int playerId)
    {
        if (playerInventory.ContainsKey(playerId))
        {
            TakeItemFromPlayerById(playerId);
            playerInventory[playerId] = itemType;
        }
        else
        {
            playerInventory.Add(playerId, itemType);
        }

        itemHUDList.Find(i => i.PlayerId == playerId).InstantiateItem(itemType);
    }

    public void TakeItemFromPlayerById(int playerId)
    {
        if (playerInventory.ContainsKey(playerId))
            playerInventory.Remove(playerId);

        itemHUDList.Find(i => i.PlayerId == playerId).DestroyItem();
    }

    public ItemTypes GetPlayerItemType(int playerId)
    {
        if (playerInventory.ContainsKey(playerId))
            return playerInventory[playerId];
        else
            return ItemTypes.Granade;
    }

    public void ExecuteItem(int playerId)
    {
        if (!playerInventory.ContainsKey(playerId))
            return;
        if (playerInventory[playerId] == ItemTypes.Shield)
        {
            SoundManager.Instance.PlaySFX("Shielded");
            return;
        }

        SoundManager.Instance.PlaySFX("UseItem");

        ItemTypes itemType = playerInventory[playerId];
        TakeItemFromPlayerById(playerId);
        switch (itemType)
        {
            case ItemTypes.Granade:
                ExecuteThrow(playerId, itemType);
                break;
            case ItemTypes.EMP:
                ExecuteThrow(playerId, itemType);
                break;
            case ItemTypes.Trap:
                ExecutePlace(playerId, itemType);
                break;
            case ItemTypes.Battery:
                ExecutePlace(playerId, itemType);
                break;
            case ItemTypes.Drain:
                ExecutePlace(playerId, itemType);
                break;
            default:
                break;
        }
    }

    private void ExecuteThrow(int playerId, ItemTypes itemType)
    {
        PlayerController playerController = GameController.Instance.GetPlayerControllerById(playerId);
        Item throwable = Instantiate(itemList.Find(i => i.itemType == itemType));

        throwable.PlayerId = playerId;

        throwable.transform.position = playerController.GetComponent<SpriteRenderer>().flipX ? 
            playerController.playerView.rightSpawnPoint.transform.position : playerController.playerView.leftSpawnPoint.transform.position;

        float force = playerController.GetComponent<SpriteRenderer>().flipX ? ThrowableForce : -ThrowableForce;

        throwable.GetComponent<Rigidbody2D>().AddForce(new Vector2(force, 0));
    }

    private void ExecutePlace(int playerId, ItemTypes itemType)
    {
        PlayerController playerController = GameController.Instance.GetPlayerControllerById(playerId);
        Item placeble = Instantiate(itemList.Find(i => i.itemType == itemType));

        placeble.PlayerId = playerId;

        placeble.transform.position = playerController.playerView.transform.position;
    }
}
