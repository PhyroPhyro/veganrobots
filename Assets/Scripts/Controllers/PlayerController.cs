﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public delegate void OnEnergyAmount(float amount);
    public event OnEnergyAmount OnEnergyAmountChanged;

    public int PlayerId;

    [SerializeField]
    private float energyAmount;
    public float EnergyAmount
    {
        get
        {
            return energyAmount;
        }
        set
        {
            energyAmount = value;
            if(OnEnergyAmountChanged != null)
                OnEnergyAmountChanged(value);
        }
    }
    public bool IsCharging;
    public bool isHit;
    public int LastCheckpoint;
    public PlayerView playerView;

    private void Start()
    {
        playerView.OnStartRecharge += () => { IsCharging = true; };
        playerView.OnStopRecharge += () => { IsCharging = false; };
        playerView.OnStopRecharge += SetLastCheckpoint;
    }

    public void RoundStart()
    {
        energyAmount = 0;
        LastCheckpoint = 0;
        GetComponent<RechargeMeter>().rechargeSlider.value = 0;
    }

    private void FixedUpdate()
    {
        if (IsCharging)
            Charge();
        else
            Decharge();
    }

    private void Charge()
    {
        EnergyAmount += GameController.Instance.RechargeRate;

        if (energyAmount >= GameController.Instance.MaxRechargeValue)
            MatchController.Instance.RoundEnd(gameObject);

        if (LastCheckpoint + 1 < GameController.Instance.RechargeCheckpoint.Count)
        {
            if (EnergyAmount >= GameController.Instance.RechargeCheckpoint[LastCheckpoint+1])
            {
                SoundManager.Instance.PlaySFX("Checkpoint");
                SetLastCheckpoint();
            }
        }
    }

    private void SetLastCheckpoint()
    {
        for (int i = 0; i < GameController.Instance.RechargeCheckpoint.Count; i++)
        {
            if (EnergyAmount >= GameController.Instance.RechargeCheckpoint[i])
                LastCheckpoint = i;
        }
    }

    public void Decharge(bool untilCheckpoint = false)
    {
        if(untilCheckpoint)
            EnergyAmount = GameController.Instance.RechargeCheckpoint[LastCheckpoint];

        if (EnergyAmount > GameController.Instance.RechargeCheckpoint[LastCheckpoint])
            EnergyAmount -= GameController.Instance.DechargeRate;
        else
        {
            EnergyAmount = GameController.Instance.RechargeCheckpoint[LastCheckpoint];
        }
    }

    public void GetHit(Vector2 fromPos, float hitForce, float duration = 0)
    {
        if(ItemController.Instance.GetPlayerItemType(PlayerId) != ItemTypes.Shield)
        {
            isHit = true;
            playerView.GetHit(fromPos, hitForce);
            StartCoroutine(StunFromHit(duration));
        }
        else
        {
            ItemController.Instance.TakeItemFromPlayerById(PlayerId);
        }
    }

    public void DisarmPlayer()
    {
        ItemController.Instance.TakeItemFromPlayerById(PlayerId);
    }

    IEnumerator StunFromHit(float duration = 0)
    {
        yield return new WaitForSeconds(duration != 0 ? duration : GameController.Instance.TimeStunedFromHit);
        isHit = false;
        playerView.RecoverFromHit();
    }
}
