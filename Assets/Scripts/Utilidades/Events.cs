﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Events  {

	public delegate void simpleEvent();
	public delegate string simpleEventString();
	public delegate void simpleEventInt(int index);
	
}
