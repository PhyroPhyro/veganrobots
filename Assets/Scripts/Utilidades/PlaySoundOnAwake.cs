﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnAwake : MonoBehaviour
{

    public string SoundName;
    public bool IsSFX;

    private void OnEnable()
    {
        if (IsSFX)
            SoundManager.Instance.PlaySFX(SoundName);
        else
            SoundManager.Instance.PlayBGM(SoundName);
    }
}