﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteTrigger : MonoBehaviour
{
    public delegate void OnTrigger(Collider2D collision);
    public event OnTrigger OnTriggerEnter, OnTriggerExit;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(OnTriggerEnter != null)
            OnTriggerEnter(collision);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(OnTriggerExit != null)
            OnTriggerExit(collision);
    }
}
