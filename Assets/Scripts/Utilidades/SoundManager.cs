﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SoundSFX
{
    public string SoundName;
    public List<AudioClip> SoundClipList;
}

public class SoundManager : Singleton<SoundManager>
{

    public List<SoundSFX> SoundList;
    public List<SoundSFX> MusicList;
    public List<SoundSFX> AmbienceList;
    public AudioSource SfxSource;
    public AudioSource BgmSource;
    public AudioSource AmbienceSource;

    public bool IsMuted;

    public void PlaySFX(string sfxName, bool isLoop = false)
    {
        //SfxSource.Stop();
        SoundSFX sound = SoundList.Find(s => s.SoundName == sfxName);

        if(!string.IsNullOrEmpty(sound.SoundName) && sound.SoundClipList.Count > 0)
        {
            AudioClip clip = sound.SoundClipList[Random.Range(0, sound.SoundClipList.Count - 1)];

            if(clip != null)
            {
                AudioSource source = gameObject.AddComponent<AudioSource>();
                source.loop = isLoop;
                source.PlayOneShot(clip);
                StartCoroutine(SoundSourceRoutine(source));
            }
        }
    }

    IEnumerator SoundSourceRoutine(AudioSource source)
    {
        while (source.isPlaying)
            yield return null;
        Destroy(source);
    }

    public void PlayBGM(string bgmName)
    {
        BgmSource.Stop();

        SoundSFX sound = MusicList.Find(s => s.SoundName == bgmName);

        if (!string.IsNullOrEmpty(sound.SoundName) && sound.SoundClipList.Count > 0)
        {
            AudioClip clip = sound.SoundClipList[Random.Range(0, sound.SoundClipList.Count - 1)];

            if (clip != null)
                BgmSource.PlayOneShot(clip);

            BgmSource.loop = true;
        }
    }

    public void PlayAmbience(string bgmName)
    {
        AmbienceSource.Stop();
        SoundSFX sound = AmbienceList.Find(s => s.SoundName == bgmName);

        if (!string.IsNullOrEmpty(sound.SoundName) && sound.SoundClipList.Count > 0)
        {
            AudioClip clip = sound.SoundClipList[Random.Range(0, sound.SoundClipList.Count - 1)];

            if (clip != null)
                AmbienceSource.PlayOneShot(clip);

            AmbienceSource.loop = true;
        }
    }

    public void MuteGame()
    {
        IsMuted = !IsMuted;
        if (IsMuted)
        {
            SfxSource.volume = 0;
            BgmSource.volume = 0;
            AmbienceSource.volume = 0;
        }
        else
        {
            SfxSource.volume = 0.7f;
            BgmSource.volume = 0.5f;
            AmbienceSource.volume = 0.3f;
        }
    }
}