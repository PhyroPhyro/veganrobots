﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneButton : MonoBehaviour {

    public string SceneName;

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(GoToScene);
    }

    public void GoToScene()
    {
        SceneManager.LoadScene(SceneName);
    }
}
