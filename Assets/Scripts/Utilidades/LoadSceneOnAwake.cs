﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneOnAwake : MonoBehaviour
{

    public string SceneName;

    private void Awake()
    {
        SceneManager.LoadScene(SceneName);
    }
}
