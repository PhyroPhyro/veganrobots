﻿public static class GameEvents {

    public delegate void GameState();
    public static event GameState OnStartGame, OnEndGame, OnPauseGame, OnGameReset, OnResumeGame;

    public static void StartGame()
    {
        //OnStartGame?.Invoke();
    }

    public static void EndGame()
    {
       // OnEndGame?.Invoke();
    }

    public static void PauseGame()
    {
     //   OnPauseGame?.Invoke();
    }

    public static void ResetGame()
    {
     //   OnGameReset?.Invoke();
    }

    public static void ResumeGame()
    {
      //  OnResumeGame?.Invoke();
    }
}
