﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonSFX : MonoBehaviour, ISelectHandler, IPointerEnterHandler
{

    public string SFXName;
    public string SelectedSFX;

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(PlaySFX);
    }

    public void PlaySFX()
    {
        SoundManager.Instance.PlaySFX(SFXName);
    }

    public void PlaySFX(string sfxName)
    {
        SoundManager.Instance.PlaySFX(sfxName);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        PlaySFX(SelectedSFX);
    }

    public void OnSelect(BaseEventData eventData)
    {
        PlaySFX(SelectedSFX);
    }
}