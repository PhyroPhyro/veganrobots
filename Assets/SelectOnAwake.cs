﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectOnAwake : MonoBehaviour
{
    public Button button, postButton;

    private void Start()
    {
        button.Select();
    }

    public void SelectOtherButton()
    {
        postButton.Select();
    }
}
